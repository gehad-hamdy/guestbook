$(document).ready(function () {
    $("#name").on({
        change: function () {
            var name = $("#name").val();
            if (name === "" || name.length <= 3) {
                $("div#status_name").html("Please enter Valid name with length not less than 3 litters").css("color", "red");
                $('#email').prop('readonly', true);
                $('#message').prop('readonly', true);
                $('#success').prop('readonly', true);
            } else {
                $("div#status_name").hide();
                $('#email').prop('readonly', false);
                $('#message').prop('readonly', false);
                $('#success').prop('readonly', false);
            }
        }
    });

    $("#message").on({
        change: function () {
            var message = $("#message").val();
            if (message === "" || message.length <= 5) {
                $("div#status_message").html("Please enter Valid message with length not less than 5 litters").css("color", "red");
                $('#email').prop('readonly', true);
                $('#name').prop('readonly', true);
                $('#success').prop('readonly', true);
            } else {
                $("div#status_message").hide();
                $('#email').prop('readonly', false);
                $('#name').prop('readonly', false);
                $('#success').prop('readonly', false);
            }
        }
    });

    $("#email").on({
        change: function () {
            var email = $("#email").val();
            if (!validateEmail(email) || email === "") {
                $("div#status_email").html("Please enter Valid Email Ex- gh@example.com").css("color", "red");
                $('#message').prop('readonly', true);
                $('#name').prop('readonly', true);
                $('#success').prop('readonly', true);
                $('#facebook').prop('readonly', true);
            } else {
                $("div#status_email").hide();
                $('#message').prop('readonly', false);
                $('#name').prop('readonly', false);
                $('#success').prop('readonly', false);
                $('#facebook').prop('readonly', false);
            }
        }
    });

    function validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test($email);
    }

    $('#guest').submit(function (event) {
        // get the form data
        var formData = {
            class: 'DBConnection',
            function: 'saveData',
            'name': $("#name").val(),
            'email': $("#email").val(),
            'message': $("#message").val()
        };
        //post the controller url
        var postURL = "../controller/DBConnection.php";
        // process the form
        $.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: postURL, // the url where we want to POST
            data: formData, // our data object
            dataType: 'json', // what type of data do we expect back from the server
            encode: true,
            success: function (data) {
                console.log('success call ' + data);
            },
            error: function (request, status) {
                console.log('fail connection  ' + status + '  ' + request.responseText);
            }
        });
        window.location.reload();
        event.preventDefault();
    });

});