<?php
/**
 * Created by PhpStorm.
 * User: Gehad
 * Date: 9/7/2018
 * Time: 10:26 PM
 */

namespace controller;


use Exception;

class DBConnection
{
    /**
     * @return string
     * @throws Exception
     */
    public static function connection()
    {
        $conn = mysqli_connect("127.0.0.1", "root", "root", "guestbook");

        if (mysqli_connect_errno()) {
            throw new Exception("Failed to connect to MySQL: " . mysqli_connect_error());
        }
        return $conn;
    }


    /**
     * @throws Exception
     */
    public static function saveData()
    {
        $conn = self::connection();
        if ($conn) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $message = $_POST['message'];

            if ($name != '' && $email != '' && $message != '') {
                $sql = "INSERT INTO guest_book (name, email, message) VALUES ('$name', '$email','$message')";

                if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
                }
                $conn->close();
            } else {
                echo 'there was a connection problem';
            }
        } else {
            echo 'there was some fields empty please check it';
        }
    }
}


if (!empty($_POST)) {
    try {
        DBConnection::saveData();
    } catch (Exception $e) {
        echo 'error' . $e;
    }
}
