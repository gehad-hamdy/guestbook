<?php
/**
 * Created by PhpStorm.
 * User: Gehad
 * Date: 9/7/2018
 * Time: 10:46 PM
 */

namespace controller;


class CronJob
{
    /**
     * @throws \Exception
     */
    public static function checkWord()
    {
        try {
            $sqlUpdate = "";
            $dbConn = DBConnection::connection();
            if ($dbConn) {
                $sql = "SELECT * from guest_book where status = 'NEW'";
                $result = $dbConn->query($sql);
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        if (strpos($row['message'], 'baddd') !== false) {
                            /* update the message status to be rejected*/
                            $sqlUpdate = "Update guest_book set status = 'REJECTED' where id = " . $row['id'];
                        } else {
                            /* update the message status to be accepted*/
                            $sqlUpdate = "Update guest_book set status = 'ACCEPTED' where id = " . $row['id'];
                        }

                        /*run the query results*/
                        if ($dbConn->query($sqlUpdate) === TRUE) {
                            echo " <br> Record updated successfully";
                        } else {
                            echo "Error updating record: " . $dbConn->error;
                        }
                    }
                } else {
                    echo "<br> No data in Cron job";
                }
            }
        } catch (\Exception $e) {
        }

    }
}