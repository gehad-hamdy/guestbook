<?php
/**
 * Created by PhpStorm.
 * User: gehad
 * Date: 14/09/18
 * Time: 06:37 م
 */

namespace controller;

include 'DBConnection.php';

class GuestBook
{
    /**
     * @return mixed
     * @throws \Exception
     */
    public static function getMessage()
    {
        try{
        $dbConn = DBConnection::connection();
        if ($dbConn) {
            $sql = "SELECT * from guest_book where status = 'ACCEPTED'";
            $result = $dbConn->query($sql);

            return $result;
        }
        }catch(\Exception $e){
            throw new \Exception('Error when retrieve message' . $e);
        }
    }
}

